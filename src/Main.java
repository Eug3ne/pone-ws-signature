import org.apache.wss4j.common.WSEncryptionPart;
import org.apache.wss4j.common.crypto.Crypto;
import org.apache.wss4j.common.crypto.CryptoFactory;
import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.common.token.BinarySecurity;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.engine.WSSConfig;
import org.apache.wss4j.dom.message.WSSecHeader;
import org.apache.wss4j.dom.message.WSSecSignature;
import org.apache.wss4j.dom.message.WSSecUsernameToken;
import org.apache.xml.security.utils.Constants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;


// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) throws Exception {
        WSSConfig.init();
        org.apache.xml.security.Init.init();

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse("/home/eug3ne/work/pone-header/src/test.xml");
        String password = "BfsMZoJZRRbQ";
        InputStream inStream = new FileInputStream("/home/eug3ne/work/pone-header/src/certs/Podmiot_leczniczy_590-wss.p12");

        KeyStore ks = KeyStore.getInstance("PKCS12");
        ks.load(inStream, password.toCharArray());

        String alias = ks.aliases().nextElement();
        PrivateKey privateKey = (PrivateKey) ks.getKey(alias, password.toCharArray());
        X509Certificate certificate = (X509Certificate) ks.getCertificate(alias);

        Crypto crypto = createCrypto();
        // Create the WSSE header
        WSSecHeader secHeader = new WSSecHeader(doc);
        secHeader.insertSecurityHeader();

        WSSecSignature signature = new WSSecSignature(secHeader);
        signature.setKeyIdentifierType(WSConstants.BST_DIRECT_REFERENCE);
        signature.setUserInfo(alias, password);
        signature.setUseSingleCertificate(true);
        signature.prependBSTElementToHeader();
        signature.setDigestAlgo(Constants.ALGO_ID_DIGEST_SHA1);
        signature.setSignatureAlgorithm(WSConstants.RSA_SHA1);
        signature.setSigCanonicalization(WSConstants.C14N_EXCL_OMIT_COMMENTS);

        List<WSEncryptionPart> references = new ArrayList<>();
        String referenceId = "id-B5EDCA695D8DF0E78C169278517394311";
        WSEncryptionPart encryptPart = new WSEncryptionPart(referenceId);
        String referenceId2 = "id-B0E9DEFF945B150A5616933180007759";

        WSEncryptionPart encryptPart2 = new WSEncryptionPart(referenceId2);
        references.add(encryptPart2);
        references.add(encryptPart);

        signature.getParts().addAll(references);


        Document signedDocument = signature.build(crypto);

//        secHeader.getSecurityHeaderElement().insertBefore(binarySecurity.getElement(), secHeader.getSecurityHeaderElement().getFirstChild());
        toString(doc);
    }
    private static void toString(Document newDoc) throws Exception{
        DOMSource domSource = new DOMSource(newDoc);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);
        System.out.println(sw.toString());
    }

    private static Crypto createCrypto() throws Exception {
        // Load the properties for the crypto provider
        Properties properties = new Properties();
        properties.setProperty("org.apache.ws.security.crypto.provider", "org.apache.ws.security.components.crypto.Merlin");

        // Load the keystore
        String password = "BfsMZoJZRRbQ";
        String keystoreFile = "/home/eug3ne/work/pone-header/src/certs/Podmiot_leczniczy_590-wss.p12";
        String keystoreType = "PKCS12";

        KeyStore ks = KeyStore.getInstance(keystoreType);
        try (InputStream keystoreStream = new FileInputStream(keystoreFile)) {
            ks.load(keystoreStream, password.toCharArray());
        }

        properties.setProperty("org.apache.ws.security.crypto.merlin.keystore.type", keystoreType);
        properties.setProperty("org.apache.ws.security.crypto.merlin.keystore.password", password);
        properties.setProperty("org.apache.ws.security.crypto.merlin.keystore.file", keystoreFile);

        // Create the Crypto object using the properties
        return CryptoFactory.getInstance(properties);
    }
}